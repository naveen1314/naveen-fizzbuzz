import React from 'react';
import { shallow } from 'enzyme';
import FizzBuzz from '../components/FizzBuzz.js'

it("conatins Prev button", () => {
    const wrapper = shallow(<FizzBuzz />);
    wrapper.setState({ total: 20 })
    expect(wrapper.find("button").length).toEqual(0);
})

it("conatins Next button", () => {
    const wrapped = shallow(<FizzBuzz />);
    wrapped.setState({ count: 21 })
    expect(wrapped.find("button").length).toEqual(1);
})



