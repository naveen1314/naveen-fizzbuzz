import React from 'react';
import { shallow, mount } from 'enzyme';
import Main from '../components/Main';

it("contains input field and button", () => {
  const wrapped = shallow(<Main />)
  expect(wrapped.find("button").length).toEqual(1);
  expect(wrapped.find("input").length).toEqual(1);
});

it("The length of the list equal to input Number", () => {
  const wrapper = mount(<Main />);
  wrapper.setState({ number: 15, showList: false });
  wrapper.find('button').first().simulate('click');
  wrapper.unmount();
})

