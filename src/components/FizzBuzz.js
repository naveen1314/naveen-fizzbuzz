import React, { Component } from 'react';
import '../App.css';
import { fizz, buzz, Fizz, Buzz, wuzz, wizz, countsVal } from './fizzBuzzConsts.js'
class FizzBuzz extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: countsVal
        }
    }
    nextPage = (e) => {
        this.setState({
            count: this.state.count + countsVal
        })
    }
    prevPage = (e) => {
        this.setState({
            count: this.state.count - countsVal
        })
    }
    fizzbuzz() {
        const { total } = this.props;
        var fizzBuzzes = [];
        for (let i = this.state.count - (countsVal - 1); i <= total && i <= this.state.count; i++) {
            var showcolor = "", showtext = "";
            if (new Date().getDay() === 3) {
                if (i % 3 === 0) {
                    showcolor = fizz;
                    showtext = wizz;
                }
                if (i % 5 === 0) {
                    showcolor += buzz;
                    showtext += wuzz;
                }
                break;
            } else {
                if (i % 3 === 0) {
                    showcolor = fizz;
                    showtext = Fizz
                }
                if (i % 5 === 0) {
                    showcolor += buzz;
                    showtext += Buzz;
                }
            }
            if (!showcolor) {
                showcolor = 'num'
                showtext = i;

            }
            fizzBuzzes.push(<span className={showcolor}>{showtext}</span>);
        }
        return fizzBuzzes;
    }

    render() {
        var listitems = this.fizzbuzz().map(function (elem, i) {
            return (
                <div key={i}>{elem}</div>
            );
        })
        return (
            <div className="result">
                {listitems}
                {this.state.count > countsVal ? <button className="btn btn-primary space" onClick={this.prevPage}>Prev</button> : null}
                {this.state.count < this.props.total ? <button className="btn btn-primary space" onClick={this.nextPage}>Next</button> : null}
            </div>
        );
    }
};

export default FizzBuzz;