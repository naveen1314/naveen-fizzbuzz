import React, { Component } from 'react';
import FizzBuzz from './FizzBuzz.js';
import { text, text2 } from './fizzBuzzConsts.js'
import '../App.css';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputvalue: '',
            showList: false,
        }
        this.printNumbers = this.printNumbers.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.setState({
            inputvalue: e.target.value,
            showList: false
        })
    }
    printNumbers() {
        const { inputvalue } = this.state;
        if ((parseInt(inputvalue)) > 0 && (parseInt(inputvalue) < 1001)) {
            var pattern = /^[1-9]\d*$/;
            if (pattern.test(inputvalue)) {
                this.setState({ showList: true })
            } else {
                alert(text);
            }
        } else {
            alert(text2)
        }
    }
    render() {
        return (
            <div className="App">
                <h1>FizzBuzz Example</h1><br />
                <input className="input-box" min={1} max={100} placeholder="Please Enter Number between 1 to 1000"
                    value={this.state.inputvalue} onChange={this.handleChange} />
                <br />
                <button className="btn btn-primary" onClick={this.printNumbers}>Find</button>
                {this.state.showList ? <div>
                    <FizzBuzz total={this.state.inputvalue} />
                </div>
                    : null}
            </div>
        );
    }
}

export default Main;